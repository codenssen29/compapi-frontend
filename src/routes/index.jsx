import { RouterProvider, createBrowserRouter } from "react-router-dom";
import { useAuth } from "../provider/authProvider";
import { ProtectedRoute } from "./ProtectedRoute";
import Login from "../pages/Login";
import Logout from "../pages/Logout";
import Companies from "../pages/Companies";
import Profil from "../pages/Profil";
import Home from "../pages/Home";
import Header from "../components/Header";
import toast, { Toaster } from "react-hot-toast";
import Footer from "../components/Footer";
import GooglePlace from "../pages/GooglePlace";

const Routes = () => {
  const { token } = useAuth();

  // Define public routes accessible to all users
  const routesForPublic = [
    {
      path: "/service",
      element: <div>Service Page</div>,
    },
    {
      path: "/about-us",
      element: <div>About Us</div>,
    },
    {
      path: "/login",
      element: (
        <>
          <Toaster />
          <Login toast={toast} />
        </>
      ),
    },
  ];

  // Define routes accessible only to authenticated users
  const routesForAuthenticatedOnly = [
    {
      path: "/",
      element: <ProtectedRoute />, // Wrap the component in ProtectedRoute
      children: [
        {
          path: "",
          element: (
            <>
              <Header />
              <Home />
              <Footer />
            </>
          ),
        },
        {
          path: "/companies",
          element: (
            <>
              <Toaster />
              <Header toast={toast} />
              <Companies />
            </>
          ),
        },
        {
          path: "/googleplaces",
          element: (
            <>
              <Toaster />
              <Header toast={toast} />
              <GooglePlace />
            </>
          ),
        },
        {
          path: "/profil",
          element: (
            <>
              <Toaster />
              <Header toast={toast} />
              <Profil />
            </>
          ),
        },
        {
          path: "/logout",
          element: <Logout />,
        },
      ],
    },
  ];

  // Define routes accessible only to non-authenticated users
  const routesForNotAuthenticatedOnly = [
    {
      path: "/",
      element: (
        <>
          <Header />
          <Home />
          <Footer />
        </>
      ),
    },
  ];

  // Combine and conditionally include routes based on authentication status
  const router = createBrowserRouter([
    ...routesForPublic,
    ...(!token ? routesForNotAuthenticatedOnly : []),
    ...routesForAuthenticatedOnly,
  ]);

  // Provide the router configuration using RouterProvider
  return <RouterProvider router={router} />;
};

export default Routes;
