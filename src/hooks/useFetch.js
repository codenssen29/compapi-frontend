import { useEffect, useState } from "react";

export function useFetch(url, options = {}) {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState(null);
  const [errors, setErrors] = useState(null);

  useEffect(() => {
    setLoading(true);
    setData(null);
    fetch(url, {
      ...options,
      headers: {
        "x-access-token": localStorage.getItem("token"),
        ...options.headers,
      },
    })
      .then((response) => {
        if (!response.ok) {
          // Si la réponse n'est pas OK (code 200), nous générons une erreur pour passer dans le bloc `catch`
          throw new Error(`Erreur ${response.status}: ${response.statusText}`);
        }
        return response.json();
      })
      .then((data) => {
        setErrors(null);
        setData(data);
      })
      .catch((err) => {
        setData(null);
        setErrors(err);
      })
      .finally(() => setLoading(false));
  }, [url]);

  return { loading, data, errors };
}
