import { Link } from "react-router-dom";

function Footer() {
  return (
    <div className="bg-rasinblack px-10 py-2 flex flex-row justify-center gap-10 mt-10">
      <Link className="default-link" to="/mention-legales">
        Mention Légales
      </Link>
      <span className="text-whitesmoke text-xl">CODYCOM - 2024</span>
    </div>
  );
}

export default Footer;
