import ReactDOM from "react-dom";

const EmailModal = ({ emails, onClose, onSelect }) => {
  return ReactDOM.createPortal(
    <div
      style={{
        position: "fixed",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
        backgroundColor: "rgba(0, 0, 0, 0.5)", // Fond semi-transparent
        zIndex: 1000,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <div
        style={{
          background: "white",
          padding: "20px",
          borderRadius: "8px",
          width: "90%", // Largeur relative à l'écran
          height: "80%", // Hauteur relative à l'écran
          display: "flex",
          flexDirection: "column",
          overflowY: "auto", // Scroll si contenu dépasse
        }}
      >
        <h3 style={{ textAlign: "center" }}>Choisir email</h3>
        <ul
          style={{ listStyle: "none", padding: 0, flex: 1, overflowY: "auto" }}
        >
          {emails.map((email, index) => (
            <li key={index} style={{ marginBottom: "10px" }}>
              <button
                style={{
                  background: "blue",
                  color: "white",
                  border: "none",
                  padding: "10px",
                  cursor: "pointer",
                  borderRadius: "4px",
                  width: "100%",
                  textAlign: "center",
                }}
                onClick={() => onSelect(email)}
              >
                {email}
              </button>
            </li>
          ))}
        </ul>
        <button
          onClick={onClose}
          style={{
            marginTop: "10px",
            padding: "10px 20px",
            cursor: "pointer",
            borderRadius: "4px",
            background: "red",
            color: "white",
            border: "none",
            alignSelf: "center",
          }}
        >
          Supprimer tous les emails
        </button>
        <button
          onClick={onClose}
          style={{
            marginTop: "10px",
            padding: "10px 20px",
            cursor: "pointer",
            borderRadius: "4px",
            background: "gray",
            color: "white",
            border: "none",
            alignSelf: "center",
          }}
        >
          Annuler
        </button>
      </div>
    </div>,
    document.body // Rend la modale dans le body
  );
};

export default EmailModal;
