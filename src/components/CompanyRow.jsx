function CompanyRow({ company }) {
  return (
    <div className="bg-lightgrey w-10/12 mt-1 flex flex-row items-center justify-between">
      <div className="text-sm p-1 w-20 text">{company.SIREN}</div>
      <div className="text-sm font-bold p-1 w-1/6">{company.denomination}</div>
      <div className="text-sm p-1 w-12">{company.code_ape}</div>
      <div className="text-sm p-1 w-12">{company.code_postal}</div>
      <div className="text-sm p-1 w-1/4">{company.ville}</div>
      <div className="text-sm p-1 w-20">{company.date_immatriculation}</div>
      <button className="bg-bluecs text-whitesmoke hover:bg-rasinblack px-2 text-sm font-bold rounded transition duration-300">
        Info
      </button>
    </div>
  );
}

export default CompanyRow;
