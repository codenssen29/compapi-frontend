import { Link, NavLink } from "react-router-dom";
import { useAuth } from "../provider/authProvider";
import { useNavigate } from "react-router-dom";
import Logo from "../assets/logo_v0.png";

function Header({ toast }) {
  const { setAuthData, token } = useAuth();

  const navigate = useNavigate();
  function handleLogout() {
    console.log("logout");
    toast.success("Vous êtes maintenant déconnecté", { icon: "❌" });
    setTimeout(() => {
      console.log("logout");
      setAuthData();
      navigate("/", { replace: true });
    }, 2000);
  }

  return (
    <>
      <div className="bg-rasinblack min-h-20 flex flex-col py-10 px-10 items-center justify-between gap-10 sm:flex-row sm:py-4">
        <Link to="/">
          <img src={Logo} className="h-20 object-contain" />
        </Link>
        {token && ( // Render buttons only if token exists
          <ul className="flex flex-row gap-10 items-center justify-center">
            <li>
              <NavLink
                to="/companies"
                className={({ isActive }) => [
                  isActive
                    ? "text-skybluecs font-bold hover:underline"
                    : " text-greycs font-bold hover:underline",
                ]}
              >
                Companies
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/googleplaces"
                className={({ isActive }) => [
                  isActive
                    ? "text-skybluecs font-bold hover:underline"
                    : " text-greycs font-bold hover:underline",
                ]}
              >
                Google API
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/profil"
                className={({ isActive }) => [
                  isActive
                    ? "text-skybluecs font-bold hover:underline"
                    : "text-greycs font-bold hover:underline",
                ]}
              >
                Profil
              </NavLink>
            </li>
            <li>
              <Link
                className="text-red-700 font-bold hover:underline"
                onClick={handleLogout}
              >
                Logout
              </Link>
            </li>
          </ul>
        )}
        {!token && (
          <Link to="/login" className="default-button">
            Login !
          </Link>
        )}
      </div>
    </>
  );
}

export default Header;
