import { useState } from "react";
import EmailModal from "./EmailModal";
import axios from "axios";

const EmailRenderer = ({ value, rowData, onUpdate }) => {
  // rowData contient les données de la ligne
  const [showModal, setShowModal] = useState(false);

  if (!value || value.length === 0) {
    return <span className="text-warmGray-400">Aucun email trouvé</span>;
  }

  const selectionEmail = async (email) => {
    const token = localStorage.getItem("token");

    try {
      console.log("Email sélectionné :", email);
      console.log("Token :", token);
      console.log("Données de la ligne :", rowData);

      const response = await axios.put(
        `http://localhost:8080/api/google/${rowData._id}`, // Utilise l'ID de la ligne
        {
          scraped_email: email,
        },
        {
          headers: {
            "x-access-token": token, // Ajoute le token dans les headers
          },
        }
      );
      console.log("Données mises à jour avec succès :", response.data);

      // Appelle la fonction de mise à jour passée en prop

      onUpdate(rowData, { scraped_email: email });

      return response.data;
    } catch (err) {
      console.log("Erreur lors de la mise à jour des données :", err);
    }
  };

  return (
    <>
      <button className="grid-button" onClick={() => setShowModal(true)}>
        {value.length === 1 ? value[0] : "🔺Choisir un email"}
      </button>
      {showModal && (
        <EmailModal
          emails={value}
          onClose={() => setShowModal(false)}
          onSelect={(email) => {
            selectionEmail(email);
            setShowModal(false);
          }}
        />
      )}
    </>
  );
};

export default EmailRenderer;
