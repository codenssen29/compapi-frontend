import { useFetch } from "../hooks/useFetch";
import { useState } from "react";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import { registerLocale } from "react-datepicker";
import { fr } from "date-fns/locale/fr";
registerLocale("fr", fr);

import { AgGridReact } from "ag-grid-react"; // AG Grid Component
import "ag-grid-community/styles/ag-grid.css"; // Mandatory CSS required by the grid
import "ag-grid-community/styles/ag-theme-quartz.css"; // Optional Theme applied to the grid

import axios from "axios";

import Loader from "../components/Loader";

import codeApe from "../data/CODENAF.json";

import { format } from "date-fns";

import { AG_GRID_LOCALE_FR } from "@ag-grid-community/locale";

function getApeLabel(code) {
  // Utilise le fichier JSON importé `codeApe`
  const ape = codeApe.find((item) => item.Code === code);

  // Retourne le label ou un message par défaut si non trouvé
  return ape ? ape.Label : "Code APE non disponible";
}

function Companies() {
  const todayDate = format(new Date(), "yyyy-MM-dd");

  const [currentUrl, setCurrentUrl] = useState(
    "http://localhost:8080/api/company/" + todayDate
  );
  const { loading, data, errors } = useFetch(currentUrl, {
    headers: {},
  });

  const pagination = true;
  const paginationPageSize = 100;
  const paginationPageSizeSelector = [10, 50, 100];

  const [date, setDate] = useState(new Date());
  const [month, setMonth] = useState("");

  // const PappersRenderer = (props) => {
  //   // Vérifie si la valeur existe et n'est pas vide
  //   if (!props.value) {
  //     return null; // Ne rien afficher si la valeur est absente ou vide
  //   }

  //   const url = `https://www.pappers.fr/entreprise/${props.value}`;
  //   return (
  //     <a className="grid-button" href={url} target="_blank" rel="noreferrer">
  //       {props.value}
  //     </a>
  //   );
  // };

  const APERenderer = (props) => {
    // Vérifie si la valeur existe et n'est pas vide
    if (!props.value) {
      return null; // Ne rien afficher si la valeur est absente ou vide
    }

    // Récupère le label du code APE
    const label = getApeLabel(props.value);

    const url = `http://localhost:8080/api/company/ape/${props.value}`;
    return (
      <>
        <button
          title={label}
          className="grid-button"
          onClick={() => setCurrentUrl(url)}
        >
          {props.value} - {label}
        </button>
      </>
    );
  };

  const DenominationRenderer = (props) => {
    if (!props.data || !props.data.SIREN || !props.value) {
      return null;
    }

    const siren = props.data.SIREN;
    const url = `https://www.pappers.fr/entreprise/${siren}`;

    return (
      <a className="grid-button" href={url} target="_blank" rel="noreferrer">
        {props.value}
      </a>
    );
  };

  const onCellValueChanged = async (params) => {
    const updatedData = params.data;

    try {
      const token = localStorage.getItem("token"); // Récupère le token du localStorage

      // Envoie une requête PUT ou PATCH à l'API pour mettre à jour les données sur le backend
      const response = await axios.put(
        `http://localhost:8080/api/company/${updatedData.SIREN}`, // URL de l'API
        {
          comment: updatedData.comment, // Les données mises à jour
          status: updatedData.status, // Les autres champs à mettre à jour
        },
        {
          headers: {
            "x-access-token": token, // Ajoute le token dans les headers
          },
        }
      );
      console.log("Données mises à jour avec succès : ", response.data);
    } catch (err) {
      console.error("Erreur lors de la mise à jour des données :", err);
    }
  };

  const statusLabels = {
    notseen: "Non vu",
    seen: "Vu",
    close: "Fermé",
    todo: "À faire",
  };

  const [colDefs, setColDefs] = useState([
    // {
    //   headerName: "SIREN",
    //   field: "SIREN",
    //   filter: true,
    //   cellRenderer: PappersRenderer, // Utilisation du PappersRenderer corrigé
    //   width: 120,
    // },

    {
      headerName: "Dénomination",
      field: "denomination",
      filter: true,
      cellRenderer: DenominationRenderer,
      width: 300,
    },
    {
      headerName: "Code APE",
      field: "code_ape",
      filter: true,
      cellRenderer: APERenderer, // Utilisation du APERenderer corrigé
      width: 400,
    },
    // { headerName: "Adresse", field: "adresse", filter: true },
    {
      headerName: "Code Postal",
      field: "code_postal",
      filter: true,
      maxWidth: 150,
    },
    { headerName: "Ville", field: "ville", filter: true },

    // {
    //   headerName: "Date d'immatriculation",
    //   field: "date_immatriculation",
    //   filter: true,
    // },

    // { headerName: "Forme Juridique", field: "forme_juridique", filter: true },

    {
      headerName: "Commentaire",
      field: "comment",
      editable: true,
      minWidth: 400,
    },
    {
      headerName: "Status",
      field: "status",
      editable: true,
      cellEditor: "agSelectCellEditor",
      cellEditorParams: {
        values: ["notseen", "seen", "close", "todo"], // Valeurs possibles dans l'éditeur
      },
      valueFormatter: (params) => statusLabels[params.value] || "Inconnu", // Affichage formaté
      valueParser: (params) => {
        const reverseLabels = {
          "Non vu": "notseen",
          Vu: "seen",
          Fermé: "close",
          "À faire": "todo",
        };
        return reverseLabels[params.newValue] || params.newValue; // Conversion lors de l'édition
      },
      cellRenderer: (params) => {
        const status = params.value;
        const style = {
          notseen: { color: "grey", icon: "🔵" },
          seen: { color: "blue", icon: "🟢" },
          close: { color: "green", icon: "✔️" },
          todo: { color: "orange", icon: "🕒" },
        }[status] || { color: "gray", icon: "❔" };

        const label = statusLabels[status] || "Inconnu";

        return (
          <div style={{ display: "flex", alignItems: "center", gap: "5px" }}>
            <span style={{ color: style.color }}>{style.icon}</span>
            <span style={{ color: style.color, fontWeight: "bold" }}>
              {label}
            </span>
          </div>
        );
      },
      width: 150,
    },
  ]);

  return (
    <div className="bg-whitesmoke">
      {" "}
      <div className="flex flex-col gap-2 justify-center content-center items-center p-10">
        <h2 className="text-3xl mb-4">
          Recherche des entreprises nouvellement immatriculées
        </h2>
        <h3 className="font-bold">Choix de la date :</h3>
        <DatePicker
          className="p-1 border border-warmGray-500 rounded-xl items-center text-center"
          dateFormat="dd/MM/yyyy"
          locale="fr"
          selected={date}
          onChange={(date) => {
            // Transformation de la date en string au format 'YYYY-MM-DD'
            console.log("date: " + date);
            const formattedDate = date.toISOString().split("T")[0];
            const year = date.getFullYear();
            const month = (date.getMonth() + 1).toString().padStart(2, "0");
            const formattedMonth = `${year}-${month}`;
            setMonth(formattedMonth);
            setCurrentUrl(
              `http://localhost:8080/api/company/date/${formattedDate}`
            );
            setDate(date);
          }}
        />
        {month == "" ? (
          <></>
        ) : (
          <button
            className="default-button m-2"
            onClick={() => {
              setMonth("");
              setCurrentUrl(`http://localhost:8080/api/company/month/${month}`);
            }}
          >
            Choisir le mois entier
          </button>
        )}
        {data && data.results ? (
          <>Nombre de résultats : {data.results}</>
        ) : (
          <></>
        )}
      </div>
      <div className="flex justify-center flex-col align items-center">
        {loading && <Loader />}
        {errors && (
          <div
            className="p-4 mb-4 text-sm text-red rounded-lg bg-lightred"
            role="alert"
          >
            <span className="font-medium">Erreur !</span> {errors.message}
          </div>
        )}
      </div>
      <div
        className="ag-theme-quartz" // applying the grid theme
        style={{ height: 600, width: "100%" }} // the grid will fill the size of the parent container
      >
        {data && data.companies && (
          <AgGridReact
            localeText={AG_GRID_LOCALE_FR}
            pagination={pagination}
            paginationPageSize={paginationPageSize}
            paginationPageSizeSelector={paginationPageSizeSelector}
            rowData={data.companies}
            columnDefs={colDefs}
            domLayout="autoHeight"
            onCellValueChanged={onCellValueChanged}
            // autoSizeStrategy={{ type: "fitCellContents" }}
            alwaysShowHorizontalScroll={true}
            getRowStyle={(params) => {
              // Appliquer une couleur de ligne en fonction de la valeur de la colonne "status"
              switch (params.data.status) {
                case "notseen":
                  return { backgroundColor: "#ffffff" }; // Blanc
                case "seen":
                  return { backgroundColor: "#d1ecf1" }; // Bleu clair
                case "close":
                  return { backgroundColor: "#B6B6B6" }; // Gris foncé
                case "todo":
                  return { backgroundColor: "#fff3cd" }; // Jaune clair
                default:
                  return null; // Pas de style par défaut
              }
            }}
          />
        )}
      </div>
    </div>
  );
}

export default Companies;
