import Logolight from "../assets/logo_light.png";
import { useState } from "react";
// import toast, { Toaster } from "react-hot-toast";
import axios from "axios";
import { useAuth } from "../provider/authProvider";
import { useNavigate } from "react-router-dom";
import Header from "../components/Header";

function Login({ toast }) {
  const { setAuthData } = useAuth();
  const navigate = useNavigate();

  const [data, setData] = useState({
    email: "",
    password: "",
  });
  const [isLoading, setIsLoading] = useState(false);

  const loginUser = async (event) => {
    event.preventDefault();

    try {
      setIsLoading(true);
      const response = await axios.post("http://localhost:8080/login/", {
        email: data.email.toLowerCase(),
        password: data.password,
      });

      if (response.data.token) {
        toast.success("Success !");
        setIsLoading(false);
        setTimeout(() => {
          setAuthData(response.data.token, response.data.userId);
          navigate("/companies"); // Make sure this path is correct
        }, 2000);
      } else {
        toast.error("Token not found in response.");
      }
    } catch (error) {
      if (error.response && error.response.status === 401) {
        toast.error("Invalid email or password!");
      } else {
        toast.error("Connection error");
      }
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <>
      <Header />
      <div className="bg-rasinblack">
        <div className="w-full h-[calc(100vh-120px)] flex flex-col gap-5 items-center justify-center">
          <div className=" w-72 flex flex-col gap-5 items-center justify-center">
            <img className="w-20" src={Logolight} />
            <h1 className="default-h1">Sign In</h1>
            <h2 className="default-h2">Sign in and start to get leads</h2>
            <form className="flex flex-col gap-5" onSubmit={loginUser}>
              <input
                className="default-input w-full"
                placeholder="Login"
                type="email"
                value={data.email}
                onChange={(event) =>
                  setData({ ...data, email: event.target.value })
                }
              />
              <input
                className="default-input w-full"
                placeholder="Password"
                type="password"
                value={data.password}
                onChange={(event) =>
                  setData({ ...data, password: event.target.value })
                }
              />
              <button disabled={isLoading} className="default-button w-full">
                Sign In
              </button>
            </form>
            <span className="text-whitesmoke">Want an account ?</span>
          </div>
        </div>
        {/* <Toaster /> */}
      </div>
    </>
  );
}

export default Login;
