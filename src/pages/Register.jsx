import axios from "axios";
import { useState } from "react";
import toast, { Toaster } from "react-hot-toast";
// import { useNavigate } from "react-router-dom";

function Register() {
  // const navigate = useNavigate();

  const [data, setData] = useState({
    email: "",
    password: "",
    role: "user",
  });

  const registerUser = async (event) => {
    event.preventDefault();
    const { role, email, password } = data;
    try {
      const responseData = await axios.post(
        "https://localhost:8080/api/users/",
        { role, email, password }
      );
      // Handle the response data as needed
      console.log(responseData.data); // Log the data received from the server
    } catch (error) {
      console.log(error);
      toast.error("Erreur ! Cause : " + error);
    }
  };

  return (
    <div className="h-lvh bg-rasinblack flex flex-col items-center justify-center">
      <div className="default-h1">Register</div>
      <div className="w-1/3">
        <form className="flex flex-col gap-10" onSubmit={registerUser}>
          <input
            className="default-input"
            type="text"
            placeholder="Name"
            value={data.name}
            onChange={(e) => setData({ ...data, name: e.target.value })}
          />
          <input
            className="default-input"
            type="email"
            placeholder="Email"
            value={data.email}
            onChange={(e) => setData({ ...data, email: e.target.value })}
          />
          <input
            className="default-input"
            type="password"
            placeholder="Password"
            value={data.password}
            onChange={(e) => setData({ ...data, password: e.target.value })}
          />
          <button className="default-button" type="submit">
            Submit
          </button>
        </form>
        <Toaster />
      </div>
    </div>
  );
}

export default Register;
