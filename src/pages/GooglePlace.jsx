import { useState, useRef } from "react";
import { useFetch } from "../hooks/useFetch";
import Loader from "../components/Loader";

import { AgGridReact } from "ag-grid-react"; // AG Grid Component
import "ag-grid-community/styles/ag-grid.css"; // Mandatory CSS required by the grid
import "ag-grid-community/styles/ag-theme-quartz.css"; // Optional Theme applied to the grid
import { AG_GRID_LOCALE_FR } from "@ag-grid-community/locale";
import axios from "axios";
import EmailRenderer from "../components/EmailRenderer";

function GooglePlace() {
  const [currentUrl, setCurrentUrl] = useState(
    "http://localhost:8080/api/google/"
  );

  const { loading, data, errors } = useFetch(currentUrl, {
    headers: {},
  });

  const gridApiRef = useRef(null); // Référence pour l'API AG Grid

  const onGridReady = (params) => {
    gridApiRef.current = params.api; // Stocke l'API de la grille
  };

  const refreshData = async () => {
    try {
      const response = await axios.get(currentUrl, {
        headers: {},
      });
      const newData = response.data;

      // Vérifiez si l'API de la grille est prête
      if (gridApiRef.current) {
        gridApiRef.current.applyTransaction({ update: newData }); // Met à jour les données dans la grille
      }
    } catch (error) {
      console.error("Erreur lors du rafraîchissement des données :", error);
    }
  };

  const onCellValueChanged = async (params) => {
    const updatedData = params.data;

    console.log(updatedData);

    try {
      const token = localStorage.getItem("token"); // Récupère le token du localStorage

      // Envoie une requête PUT ou PATCH à l'API pour mettre à jour les données sur le backend
      const response = await axios.put(
        `http://localhost:8080/api/google/${updatedData._id}`, // URL de l'API
        {
          woodpecker: updatedData.woodpecker,
          website: updatedData.website,
        },
        {
          headers: {
            "x-access-token": token, // Ajoute le token dans les headers
          },
        }
      );
      console.log("Données mises à jour avec succès : ", response.data);
    } catch (err) {
      console.error("Erreur lors de la mise à jour des données :", err);
    }
  };

  const [colDefs, setColDefs] = useState([
    {
      headerName: "Nom",
      field: "company_name",
      filter: true,
    },
    {
      headerName: "Adresse",
      field: "formatted_address",
      cellRenderer: (params) => {
        const address = params.value;
        if (address) {
          const googleMapsUrl = `https://www.google.com/maps?q=${encodeURIComponent(
            address
          )}`;
          return (
            <a href={googleMapsUrl} target="_blank" rel="noopener noreferrer">
              {address}
            </a>
          );
        }
        return ""; // Retourne une chaîne vide si aucune adresse n'est disponible
      },
      filter: true,
      width: 400,
    },
    {
      headerName: "Site Internet",
      field: "website",
      width: 300,
      cellRenderer: (params) => {
        return (
          <a href={params.value} target="_blank" rel="noreferrer">
            {params.value}
          </a>
        );
      },
      filter: true,
      editable: true,
    },
    {
      headerName: "Email Trouvé",
      field: "scraped_email",
      filter: true,
      cellRenderer: EmailRenderer,
      cellRendererParams: (params) => ({
        rowData: params.data,
        value: params.value,
        onUpdate: async () => {
          console.log("update");
          await refreshData(); // Rafraîchit les données après une mise à jour
        },
      }),
      valueFormatter: (params) => {
        if (Array.isArray(params.value)) {
          return params.value.join(", ");
        }
        return params.value || "";
      },
    },
    {
      headerName: "Types",
      field: "types",
      valueGetter: (params) => {
        return params.data.types[0];
      },
      filter: true,
    },
    {
      headerName: "Woodpeckerized",
      cellDataType: "boolean",
      field: "woodpecker",
      editable: true,
      valueGetter: (params) => {
        // Retourne false si la valeur est null ou undefined
        return params.data.woodpecker?.status ?? false;
      },
      valueSetter: (params) => {
        // Assurez-vous de modifier la structure pour inclure `woodpecker`
        if (!params.data.woodpecker) {
          params.data.woodpecker = {};
        }
        params.data.woodpecker.status = params.newValue;
        return true; // Indique à AG Grid que la valeur a bien été modifiée
      },
      cellRenderer: (params) => {
        // Affiche un check ou une case vide, utile pour personnaliser l'affichage
        return params.value ? "✔️" : "❌";
      },
    },
  ]);

  const pagination = true;
  const paginationPageSize = 100;
  const paginationPageSizeSelector = [10, 50, 100];

  return (
    <div className="bg-whitesmoke">
      <div className="flex flex-col gap-2 justify-center content-center items-center p-10">
        <h2 className="text-3xl mb-4">
          Recherche des entreprises via Google API
        </h2>
      </div>
      <div className="flex justify-center flex-col align items-center">
        {loading && <Loader />}
        {errors && (
          <div
            className="p-4 mb-4 text-sm text-red rounded-lg bg-lightred"
            role="alert"
          >
            <span className="font-medium">Erreur !</span> {errors.message}
          </div>
        )}
      </div>
      <div className="ag-theme-quartz" style={{ height: 600, width: "100%" }}>
        {data && (
          <AgGridReact
            localeText={AG_GRID_LOCALE_FR}
            pagination={pagination}
            paginationPageSize={paginationPageSize}
            paginationPageSizeSelector={paginationPageSizeSelector}
            rowData={data}
            columnDefs={colDefs}
            domLayout="autoHeight"
            onGridReady={onGridReady} // Lie l'API de la grille
            stopEditingWhenCellsLoseFocus={true}
            getRowId={(params) => params.data._id}
            onCellValueChanged={onCellValueChanged}
          />
        )}
      </div>
    </div>
  );
}

export default GooglePlace;
