import Logo from "../assets/logo_v0.png";

function Home() {
  return (
    <>
      <div className="bg-rasinblack w-full flex flex-col gap-5">
        <div className="mx-auto px-4 sm:px-6 lg:px-8 max-w-7xl gap-16 flex flex-col">
          <div className="p-10 text-wrap text-center flex flex-col items-center gap-10 sm:px-40">
            <img className="h-40 object-contain" src={Logo} />
            <h1 className="text-5xl font-bold tracking-tight text-warmGray-200">
              LeadLightHouse, <span className="text-skybluecs">Cherchez</span>,
              trouvez des <span className="text-skybluecs">propects</span>, sans
              effort !
            </h1>
            <h2 className="text-xl text-warmGray-200">
              Avec LeadLightHouse, la recherche de prospects n&apos;a jamais été
              aussi facile !
            </h2>
            <div className="flex flex-col gap-20 sm:flex-row">
              <button className="default-button">
                Rechercher des entreprises
              </button>
              <button className="default-button">Trouver des prospects</button>
            </div>
          </div>
          <div className="p-10 text-wrap text-center flex flex-col items-center gap-10 sm:px-40">
            <h2 className="text-2xl font-bold tracking-tight text-warmGray-200">
              Recherche des entreprises dans notre{" "}
              <span className="text-skybluecs">liste actualisée</span>
            </h2>
            <div className="flex flex-row gap-10 items-center">
              <div className="bg-bluecs rounded-xl p-10">
                <h3 className="text-2xl font-bold text-warmGray-200 mb-4">
                  Nombre d&apos;entreprise
                </h3>
                <p className="text-5xl text-warmGray-200 font-semibold">
                  64540
                </p>
              </div>
              <div className="bg-bluecs rounded-xl p-10">
                <h3 className="text-2xl font-bold text-warmGray-200 mb-4">
                  Clients potentiels
                </h3>
                <p className="text-5xl text-warmGray-200 font-semibold">9850</p>
              </div>
            </div>
            <p className="text-warmGray-200 text-xl">
              Dans un environnement concurrentiel où le temps est précieux,
              <span className="text-skybluecs">LeadLightHouse</span> se
              positionne comme un outil essentiel pour simplifier la recherche
              de prospects. Avec une base de données constamment mise à jour,
              comprenant plus de 64 500 entreprises et près de 10 000 clients
              potentiels, <span className="text-skybluecs">LeadLightHouse</span>{" "}
              vous permet d&apos;identifier facilement les opportunités
              commerciales adaptées à vos besoins.
            </p>
            <p className="text-warmGray-200 text-xl">
              Fini les recherches fastidieuses :{" "}
              <span className="text-skybluecs">LeadLightHouse</span> rend la
              prospection plus rapide, plus précise et surtout sans effort, pour
              que vous puissiez vous concentrer sur l&apos;essentiel — le
              développement de votre activité.
            </p>
          </div>
        </div>
      </div>
    </>
  );
}

export default Home;
