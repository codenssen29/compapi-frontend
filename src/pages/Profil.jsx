import { useAuth } from "../provider/authProvider";
import { useFetch } from "../hooks/useFetch";

function Profil() {
  const { userId } = useAuth();

  const { loading, data, errors } = useFetch(
    "http://localhost:8080/api/users/" + userId
  );

  if (loading) {
    return (
      <div className="flex justify-center items-center h-screen">
        <p className="text-2xl text-warmGray-200">Chargement en cours...</p>
      </div>
    );
  }

  if (errors) {
    return (
      <div className="flex justify-center items-center h-screen">
        <p className="text-2xl text-red-500">
          Erreur lors du chargement des données :{" "}
          {errors.message || "Erreur inconnue"}
        </p>
      </div>
    );
  }

  return (
    <div className="bg-rasinblack w-full flex flex-col gap-5">
      <div className="mx-auto px-4 sm:px-6 lg:px-8 max-w-7xl gap-16 flex flex-col">
        <div className="p-10 text-wrap text-center flex flex-col items-center gap-10 sm:px-40">
          <h1 className="text-5xl font-bold tracking-tight text-warmGray-200">
            Votre profil :
          </h1>

          <h2 className="text-xl text-warmGray-200">
            Voici la gestion de votre profil
          </h2>

          <p className="text-warmGray-200">
            <strong>Email :</strong> {data && data.email}
          </p>

          <p className="text-warmGray-200">
            <strong>Role :</strong> {data && data.role}
          </p>

          <p className="text-warmGray-200">
            <strong>Id :</strong> {data && data._id}
          </p>

          <div className="flex flex-col gap-20 sm:flex-row">
            <button className="default-button">Modifier</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Profil;
