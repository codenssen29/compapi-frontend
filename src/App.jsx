import AuthProvider from "./provider/authProvider";
import Routes from "./routes";

function App() {
  return (
    <>
      {/* <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/register" element={<Register />} />
      </Routes> */}
      <AuthProvider>
        <Routes />
      </AuthProvider>
    </>
  );
}

export default App;
