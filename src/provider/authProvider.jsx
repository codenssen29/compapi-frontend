import axios from "axios";
import { createContext, useContext, useEffect, useMemo, useState } from "react";

const AuthContext = createContext();

const AuthProvider = ({ children }) => {
  // State to hold the authentication token
  const [token, setToken_] = useState(localStorage.getItem("token"));
  const [userId, setUserId_] = useState(localStorage.getItem("userId"));

  // Function to set the authentication token
  const setAuthData = (newToken, newUserId) => {
    setToken_(newToken);
    setUserId_(newUserId);
  };

  useEffect(() => {
    if (token) {
      axios.defaults.headers.common["Authorization"] = "Bearer " + token;
      localStorage.setItem("token", token);
      if (userId) {
        localStorage.setItem("userId", userId);
      }
    } else {
      delete axios.defaults.headers.common["Authorization"];
      localStorage.removeItem("token");
      localStorage.removeItem("userId");
    }
  }, [token, userId]);

  // Memoized value of the authentication context
  const contextValue = useMemo(
    () => ({
      token,
      userId,
      setAuthData,
    }),
    [token, userId]
  );

  // Provide the authentication context to the children components
  return (
    <AuthContext.Provider value={contextValue}>{children}</AuthContext.Provider>
  );
};

export const useAuth = () => {
  return useContext(AuthContext);
};

export default AuthProvider;
