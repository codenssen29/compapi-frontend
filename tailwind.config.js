const colors = require("tailwindcss/colors");

module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
    "./node_modules/react-tailwindcss-datepicker/dist/index.esm.js",
  ],
  theme: {
    extend: {
      colors: {
        // Vos couleurs personnalisées
        whitesmoke: "#F1F3F0",
        rasinblack: "#262831",
        bluecs: "#2289A6",
        skybluecs: "#57DAEE",
        greycs: "#667674",
        lightgrey: "#DEE3E2",
        redcs: "#EE4B2B",
        lightred: "#F9BFB4",
        // Ajout d'autres couleurs personnalisées ici
      },
      fontFamily: {
        outfit: ['"Outfit"'],
        // Ajout de plus de familles de polices personnalisées ici
      },
      // Étendre d'autres aspects du thème ici
    },
    // Vous pouvez explicitement spécifier d'utiliser les couleurs de Tailwind par défaut ici
    // mais c'est déjà inclus lorsque vous utilisez `extend`
    colors: {
      transparent: "transparent",
      current: "currentColor",
      // Intégration des couleurs par défaut de Tailwind
      ...colors,
    },
  },
  plugins: [],
};
